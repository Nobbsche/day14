#[cfg(test)]
mod tests {

    use std::fs;

    fn read_contents_from_file(filename: String) -> Vec<String> {
        let contents = fs::read_to_string(filename)
            .expect("Something went wrong reading the file");
        contents.lines().map(|x| String::from(x) ).collect()
    }

    fn find_secific_reaction(result: String, reactions: Vec<String> ) -> String {
        let reaction : Vec<String> = reactions.into_iter().filter(|x| x.contains(result.as_str()) ).collect();
        reaction[0].clone()
    }

    #[test]
    fn test_parse_reaction() {

        let reactions = read_contents_from_file(String::from("src/day14_test_data.txt"));
        println!("reactions: {:?}", reactions);
        assert_eq!(find_secific_reaction(String::from("FUEL"), reactions), String::from("7 A, 1 E => 1 FUEL"));
    }

    
}
